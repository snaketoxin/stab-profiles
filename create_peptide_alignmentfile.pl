#!/usr/bin/perl -w
use strict;

###############################
#
# INPUT: path to file with peptide replicated signal
# OUTPUT: FASTA file with experiment info in header and peptide sequence
#

my $immufiledir = '/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/normdata/';
opendir(DH, "$immufiledir") or die "reason: $!\n";
my @tmpdir = readdir(DH);
closedir DH;

my @fileList = ();

#foreach my $file (@tmpdir) {
#	push(@fileList, $file) if $file =~ m/\_replica\.txt$/;
#}
#### Updated after mulitple chip data is combined:
foreach my $file (@tmpdir) {
	push(@fileList, $file) if $file =~ m/\_minusBuffer\_norm\.txt$/;
}


my ($outfile, $fileID, $expID, $antivenom, $sequence, $cons, $signal) = ('', '', '', '', '', '', '');
my $count = 1;

for (my $i = 0; $i < $#fileList+1; $i++) {
	$fileID = $fileList[$i];
	print "Evaluating: ", $fileID, "\n";
	
	$outfile = substr($fileID, 0, length($fileID)-9).".fasta";	## only keep antivenom name
	
	#$expID = substr($fileID, 0, 10);
	$count = 1;
	open (IN, '<', $immufiledir.$fileID) or die "can't read file, reson: $!\n";
	open (OUT, '>', "/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/peptide/".$outfile) or die "can't write file, reson: $!\n";
	
	while (defined (my $line = <IN>)) {
		chomp $line;
	
		if ($line =~ m/^SEQ\s+Antivenom/) {
			#next;
		
		} elsif ($line =~ m/\d+$/) {
			my @tmp = split("\t", $line);
			$antivenom = $tmp[1];
			$cons = $tmp[2];
			$sequence = $tmp[0];
			$signal = $tmp[4];
		
		#if ($signal > 0) {
		
			print OUT ">", $antivenom, "_", $signal, " AV = ", $antivenom, " SIG = ", $signal, " #",$count,"\n";
			print OUT $sequence, "\n";
		
		#} 
			$count = $count+1;
	
		}
	
	}
	close IN;
	close OUT;
	($outfile, $fileID, $expID, $antivenom, $sequence, $cons, $signal) = ('', '', '', '', '', '', '');
	
}



__END__
my $outfile = "FASTAtest.fsa";
open (IN, '<', $file) or die;
open (OUT, '>', $outfile) or die;

my $expID = substr($file, 0, 10);
my $antivenom = '';
my $count = 1;
my $sequence = '';
my $cons = '';
my $signal = '';

while (defined (my $line = <IN>)) {
	chomp $line;
	
	if ($line =~ m/^SEQ\s+Antivenom/) {
		next;
		
	} elsif ($line =~ m/\d+$/) {
		my @tmp = split("\t", $line);
		$antivenom = $tmp[1];
		$cons = $tmp[2];
		$sequence = $tmp[0];
		$signal = $tmp[4];
		
		#if ($signal > 0) {
		
			print OUT ">".$expID."_".$antivenom."_".$signal."_#".$count,"\n";
			print OUT $sequence, "\n";
		
		#} 
		$count = $count+1;
	
	}
	
}

close IN;
close OUT;
