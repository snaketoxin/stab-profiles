#!/usr/bin/perl -w
use strict;
use Statistics::Basic::StdDev;
use Statistics::Basic::Median;
use Statistics::Basic::Mean;

##################################### Pan Africa study - Roche data
# Script to perform normalisation on microarray peptide:antibody interaction signal
#
# INPUT: file with signal information, probe information and location. The folder is composed of multiple data files, one file representing a chip evaluated with a specific antivenom serum
# PROCESS: log transform data, perform normaliaiton
# OUTPUT: signal information, normalisation and replication number with corresponding mean score
#
#
# All antivenom data are normalised based on RANDOM peptides for the given chip
# Computed mean and SD values are used to normalise all individual chip sektors
# Buffer sektors are normalised based on antivenom RANDOM mean+SD values

my $immufiledir = '';
my $venomfile = '';
my $chipID = '';
if ($#ARGV < 2) {
   print "input directory for roche signal file: ";
   $immufiledir = <STDIN>;		#"/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/raw/data_for_normalisation/";
   chomp $immufiledir;
   
   print "input roche antivenom map file: ";
   $venomfile = <STDIN>;		#"/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/raw/mapfile.txt";
   chomp $venomfile;
   
   print "input chip id: ";
   $chipID = <STDIN>;
   chomp $chipID;
    
} elsif ($#ARGV == 2) {
   $immufiledir = $ARGV[0];
   $venomfile = $ARGV[1];
   $chipID = $ARGV[2];
} else {
   die "Too many argument\n";
}

=put
###### ----- STEP #1 - subtract buffer signals:

my @bufferlist = ("214033_IgG_635_A12_seq.dat", "214052_IgG_635_A12_seq.dat", "214937_IgG_635_A01_seq.dat");
my $bufferfile = '';
for (my $i = 0; $i < scalar(@bufferlist); $i++) {
	my $tmp = substr($bufferlist[$i], 0, 6);
	if ($tmp eq $chipID) {
		$bufferfile = $bufferlist[$i];
	}
	
}

opendir(DH, "$immufiledir") or die "can't open directory, reason: $!\n";
my @tmpdir = readdir(DH);
closedir DH;

my @fileList = ();

foreach my $file (@tmpdir) {
	push(@fileList, $file) if $file =~ m/$chipID\S*\_seq\.dat$/;	
} 

#print @fileList, "\n";
my @dataB = ();
open (INB, '<', "/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/raw/".$bufferfile) or die "can't open buffer file, reason: $!\n";
while (defined (my $line = <INB>)) {
	chomp $line;
	
	if ($line =~ m/^#/) {
		next;
	} elsif ($line =~ m/^\[/) {
		next;
	} elsif ($line =~ m/^\n/) {
		next;
	} elsif ($line =~ m/^SEQ\_ID/) {
		next;
	} elsif ($line =~ m/^EXP\_RANDOM\_REP\d+/) {
		my @tmp = split("\t", $line);
		push(@dataB, $tmp[0], $tmp[1], $tmp[2], $tmp[3], $tmp[4], $tmp[5]);
		#	EXP_ID, probeID, sequence, signal
	} elsif ($line =~ m/^MAIN\_EXP/) {
		my @tmp = split("\t", $line);
		push(@dataB, $tmp[0], $tmp[1], $tmp[2], $tmp[3], $tmp[4], $tmp[5]);
		#	EXP_ID, probeID, sequence, signal
	} elsif ($line =~ m/^RANDOM\s+/) {		### only use the random probes for scaling 
		my @tmp = split("\t", $line);
		push(@dataB, $tmp[0], $tmp[1], $tmp[2], $tmp[3], $tmp[4], $tmp[5]);
		#	EXP_ID, probeID, sequence, signal
			
	}
}

close INB;

### Evaluate signal files and compute new signal score --> print to new file:
my ($seqid, $probeid, $seq, $x, $y, $signal, $newsignal) = ('', '', '', '', '', '', '');
my $match = '0';
for (my $j = 0; $j < scalar(@fileList); $j++) {

	open (INS, '<', $immufiledir.$fileList[$j]) or die "can't open data file, reason: $!\n";
	my $out = substr($fileList[$j], 0, 6)."_".substr($fileList[$j], 15, 3)."_minusBuffer.txt";
	open (OUT, '>', $immufiledir.$out) or die "can't write \"minusbuffer\" file, reason: $!\n";
	
	my @datatmp = @dataB;
	print "Full data list: ", scalar(@datatmp), "\n";
	
	print "Evaluating: ", $fileList[$j], "\n";
	while (defined (my $line = <INS>)) {
		chomp $line;
		
		if ($line =~ m/^#/) {
			print OUT $line, "\n";
			#next;
		} elsif ($line =~ m/^\[/) {
			print OUT $line, "\n";
			#next;
		} elsif ($line =~ m/^\n/) {
			print OUT $line, "\n";
			#next;
		} elsif ($line =~ m/^SEQ\_ID/) {
			print OUT $line, "\tbuffer\tsignal-buffer\n";
			
			#next;
		} elsif ($line =~ m/^EXP\_RANDOM\_REP\d+/) {
			my @tmp = split("\t", $line);
			$seqid = $tmp[0];
			$probeid = $tmp[1];
			$seq = $tmp[2];
			$x = $tmp[3];
			$y = $tmp[4];
			$signal = $tmp[5];
			
		} elsif ($line =~ m/^MAIN\_EXP/) {
			my @tmp = split("\t", $line);
			$seqid = $tmp[0];
			$probeid = $tmp[1];
			$seq = $tmp[2];
			$x = $tmp[3];
			$y = $tmp[4];
			$signal = $tmp[5];
			
		} elsif ($line =~ m/^RANDOM\s+/) {		### only use the random probes for scaling 
			my @tmp = split("\t", $line);
			$seqid = $tmp[0];
			$probeid = $tmp[1];
			$seq = $tmp[2];
			$x = $tmp[3];
			$y = $tmp[4];
			$signal = $tmp[5];
			
		}
		
		#print "Data to match: ", $seqid, "\t", $probeid, "\t", $seq, "\t", $x, "\t", $y, "\t", $signal, "\t", $newsignal, "\n";
		
		for (my $k = 0; $k < scalar(@datatmp); $k = $k+6) {	#(my $k = 0; $k < scalar(@datatmp); $k = $k+6) {
			#print $k, "\n";
			#print "Buffer data; ", $datatmp[$k], "\t", $datatmp[$k+1], "\t", $datatmp[$k+2], "\t", $datatmp[$k+3], "\t", $dataB[$k+4], "\t", $dataB[$k+5], "\n";
			if ($datatmp[$k] eq $seqid && $datatmp[$k+1] eq $probeid && $datatmp[$k+2] eq $seq && $datatmp[$k+3] eq $x && $datatmp[$k+4] eq $y) {
				
				$newsignal = $signal - $datatmp[$k+5];	## signal - buffer signal
				print OUT $seqid, "\t", $probeid, "\t", $seq, "\t", $x, "\t", $y, "\t", $signal, "\t", $datatmp[$k+5], "\t", $newsignal, "\n";
				
				#print "length of data list A : ", scalar(@datatmp), "\n";
				
				#$datatmp[$k] = '';
				#$datatmp[$k+1] = '';
				#$datatmp[$k+2] = '';
				#$datatmp[$k+3] = '';
				#$dataB[$k+4] = '';
				#$dataB[$k+5] = '';
				splice(@datatmp, $k, 6);
				#print "length of data list B : ", scalar(@datatmp), "\n";
				
				$k = scalar(@datatmp)+2;
				$match = 1;
			
			} 
		}
		if ($match == 0) {
			print "No match\n";
		}
		
		$match = '0';
	}
}

close OUT;
close INS;

__END__
=cut

### Scale data:



### Step 1: Load antivenom information

my @venomList = ();
open(INV, '<', $venomfile) or die "can't read venom file, reason: $!\n";		# load with antivenom <-> fileID mapping information

if (defined (my $line = <INV>)) {
	chomp $line;		# remove header
}

while (defined (my $line = <INV>)) {
	chomp $line;
	my @tmp = split(";", $line);
	if ($tmp[2] eq "1:250") {	## only store files of wan
		push(@venomList, $tmp[0], $tmp[1], $tmp[2]);	
		## Store fileID, venom name and dilution
	} 
}
close INV;

### Evaluate files in folder:

opendir(DH, "$immufiledir") or die "can't open directory, reason: $!\n";
my @tmpdir = readdir(DH);
closedir DH;

my @fileList = ();

foreach my $file (@tmpdir) {
	push(@fileList, $file) if $file =~ m/^$chipID\S*\_minusBuffer\.txt$/;  
	
} 

#print @fileList, "\n";

my $outfile = '';
my @dataX = ();
my @dataY = ();
my @list = ();
my @loglist = ();
my %replica = ();
my $antivenom = '';
my @listY = ();
my @loglistY = ();
my ($id_new, $id_old) = ('', '');

for (my $i = 0; $i < $#fileList+1; $i++) {		# 3; $i++) {	
	
	## Evaluate file and store signal information:
	open (IN, '<', $immufiledir.$fileList[$i]) or die "can't read file, reason: $!\n";
		
	while (defined (my $line = <IN>)) {
		chomp $line;
		
		if ($line =~ m/^#/) {
			#next;
		} elsif ($line =~ m/^\[/) {
			#next;
		} elsif ($line =~ m/^\n/) {
			#next;
		} elsif ($line =~ m/^SEQ\_ID/) {
			#next;
		} elsif ($line =~ m/^EXP\_RANDOM\_REP\d+/) {
			#next;
		} elsif ($line =~ m/^MAIN\_EXP/) {
			#next;
		} elsif ($line =~ m/^RANDOM\s+/) {		### only use the random probes for scaling 
			my @tmp = split("\t", $line);
			push(@dataX, $tmp[0], $tmp[1], $tmp[2], $tmp[7]);
			#	EXP_ID, probeID, sequence, signal
		
		}
	}
	
}	
	
	
print "compute norm:\n";
### compute normalisation and print files

for (my $j = 0; $j < $#dataX+1; $j = $j+4) {
	### correct for negative and 0-signal values
	## compute log transformed signals:
	my $signal = $dataX[$j+3];
	my $update;
	
	if ($signal == 0) {
		$update = 0.1;
		push(@list, $update);
		
		my $logdat = log10($update);
		push(@loglist, $logdat);
		
	} elsif ($signal < 0) {		## Remove negative signals
		# next;
		
	} elsif ($signal > 0) {
		push(@list, $signal);
		
		my $logdat = log10($signal);
		push(@loglist, $logdat);
	}
}
	
#### Normalise corrected values
my $fileMean = Statistics::Basic::mean(@list);
my $fileSD = Statistics::Basic::stddev(@list);
	
### normalise log transformed data

my $logMean = Statistics::Basic::mean(@loglist);
my $logSD = Statistics::Basic::stddev(@loglist);
		
my $normupdate = '';
my $logUpdate = '';
		
		
for (my $m = 0; $m < $#fileList+1; $m++) {
			
	my $cons = '';
	## Evaluate antivenom:
	my $fileID = substr($fileList[$m], 0, 10);
	for (my $v = 0; $v < $#venomList+1; $v = $v+3) {
		my $venom = $venomList[$v];
	
		if ($fileID eq $venom) {
			$antivenom = $venomList[$v+1];
			$cons = $venomList[$v+2];
		}
	}
				
	open (INF, '<', $immufiledir.$fileList[$m]) or die "can't read file, reason: $!\n";
	$outfile = $fileID."_".$antivenom."_minusBuffer_datanormfile.txt";
	
	while (defined (my $line = <INF>)) {
		chomp $line;
		
		if ($line =~ m/^#/) {
			#next;
		} elsif ($line =~ m/^\[/) {
			#next;
		} elsif ($line =~ m/^\n/) {
			#next;
		} elsif ($line =~ m/^SEQ\_ID/) {
			#next;
		} elsif ($line =~ m/^EXP\_RANDOM\_REP\d+/) {
			my @tmp = split("\t", $line);
			push(@dataY, $tmp[0], $tmp[1], $tmp[2], $tmp[7]);
			#	EXP_ID, probeID, sequence, signal
	
		} elsif ($line =~ m/^MAIN\_EXP/) {
			my @tmp = split("\t", $line);
			push(@dataY, $tmp[0], $tmp[1], $tmp[2], $tmp[7]);
			#	EXP_ID, probeID, sequence, signal
		
		} elsif ($line =~ m/^RANDOM\s+/) {
			#next;
		}
		
	}


	for (my $k = 0; $k < $#dataY+1; $k = $k+4) {
		### corect for negative and 0-signal values
		## compute log transformed signals:
		my $signalY = $dataY[$k+3];
		my $updateY;
		
		if ($signalY == 0) {
			$updateY = 0.1;
			push(@listY, $updateY);
			my $logdatY = log10($updateY);
			push(@loglistY, $logdatY);
			
		} elsif ($signalY < 0) {		## Remove negative signals
			# next;
			$updateY = 0.1;
			push(@listY, $updateY);
			my $logdatY = log10($updateY);
			push(@loglistY, $logdatY);
	
		} elsif ($signalY > 0) {
			push(@listY, $signalY);
		
			my $logdatY = log10($signalY);
			push(@loglistY, $logdatY);
		}
	}
	
	my $C = 0;
	open (OUT, '>', $immufiledir.$outfile) or die "can't write file, reason: $!\n";
	print OUT "EXP_ID\tPROBE\tSEQ\tantivenom\tdilution\tSIGNAL\tSIGNAL_norm\tSIGNAL_LOG10\tSIGNAL_LOG10_norm\n";

	for (my $l = 0; $l < $#listY+1; $l++) {
		$normupdate = ($listY[$l] - $fileMean)/$fileSD;
		$logUpdate = ($loglistY[$l] - $logMean)/$logSD;
		print OUT $dataY[$C], "\t", $dataY[$C+1], "\t", $dataY[$C+2], "\t", $antivenom, "\t", $cons ,"\t"; #, $dataX[$C+3], "\t";
		#	"expID\tSEQ\tAntivenom\tconsentration\tSIGNAL\t
		print OUT $listY[$l], "\t", $normupdate, "\t", $loglistY[$l], "\t", $logUpdate, "\n";	
		#       SIGNAL_corrected\tSIGNal_norm\tSIGNAL_LOG10\tSIGNAL_LOG10_norm\n";

		$replica{$dataY[$C+2]}.= "\t$logUpdate";
		
		$C = $C+4;

	}
	
	@listY = ();
	@loglistY = ();
	close INF;
	close OUT;
	
#=put
###------------------------------------------ EVALUATE NUMBER OF REPLICATES ---------------------------------###
	my $outrep = $fileID."_".$antivenom."_minusBuffer_replica.txt";
	open (OUTR, '>', $immufiledir.$outrep) or die "can't write file, reason: $!\n";
	print OUTR "SEQ\tAntivenom\tDilution\treplica\tSignalMedian\n";
	my @listZ = ();
	foreach my $key (keys %replica) {
		@listZ = split("\t", $replica{$key});
		splice(@listZ, 0, 1);		## Remove element from initial "\t" in hash key assignment
		
		my $mean = Statistics::Basic::median(@listZ);
		print OUTR $key, "\t", $antivenom, "\t", $cons, "\t", scalar(@listZ), "\t", $mean, "\n";
		@listZ = ();
	}
	close OUTR;
	$antivenom = '';
	%replica = ();
	@dataY = ();
				
}
				
############ LOG10() SUBROUTINE
sub log10 {
   my $n = shift;
   return log($n)/log(10);
}
