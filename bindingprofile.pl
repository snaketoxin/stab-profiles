#!/usr/bin/perl -w
use strict;

################################################
# by Carina Skaarup, skaarup@bioinformatics.dtu.dk
# May 2018
#
# Version 7: 
# Additionnal features: Rearange order of protein id for plotting
# 	Include subspecies in plot legend 
#
# 
######################################################


my $blastdir = "/home/people/skaarup/SNAKE/NEWANALYSIS/ANALYSIS_AUGUST/alignment/";
my $toxindir = "/home/people/skaarup/SNAKE/NEWANALYSIS/ANALYSIS_AUGUST/toxins/";
my $mapfile = "/home/people/skaarup/SNAKE/NEWANALYSIS/ANALYSIS_AUGUST/toxins/toxin_map2.txt";


#####------------------- Read blast alignment files -----------------###
opendir(DH, "$blastdir") or die "reason: $!\n";
my @tmpdir = readdir(DH);
closedir DH;

my @blastlist = ();
foreach my $file (@tmpdir) {
	push(@blastlist, $file) if $file =~ m/\_blast\.txt$/;
}
@tmpdir = ();

#####-------------------- Read toxin fasta files -------------------####
opendir(DH, "$toxindir") or die "reason: $!\n";
my @tmpdir2 = readdir(DH);
closedir DH;

my @toxinlist = ();
foreach my $file (@tmpdir2) {
	push(@toxinlist, $file) if $file =~ m/\.fasta$/;
}
@tmpdir2 = ();

#####------------------- Evaluate all combinaions
#
# For each blast alignment the represented peptide sequences are read
# The toxin fasta file is used for construction of binding profiles
#

my ($blastfile, $pfam_new) = ('', '');
my $pfam_old = 'NULL';
my ($id, $align, $gap, $mis, $chip, $genus, $antivenom, $toxinfam, $sequence) = ('', '', '', '', '', '', '', '', '');
my ($pos, $aa, $signal, $protinfo, $species, $sub, $subsub) = ('', '', '', '', '', '', '');
my $subspecies = 'NA';
my @toxin = ();
my @data = ();
my $kmer = 16;
my $outfile = "BindingprofileX.txt";
open (OUT, '>', "/home/people/skaarup/SNAKE/NEWANALYSIS/ANALYSIS_AUGUST/bindingprofile/".$outfile) or die "reason: $!\n";
print OUT "UniprotID\tGenus\tSpecies\tProteinFam\tProteinSubFam\tProteinSubSubFam\tAntivenom\tPosition_start\tPosition_end\tK_mer\tSignal\tUniprot\tpfam_genus\tpfam_species\tsub_genus\tsub_species\tsubsub_genus\tsubsub_species\n";


for (my $i = 0; $i < scalar(@blastlist);  $i++) {		#		### Evaluate one blast file at the time
	$blastfile = $blastlist[$i];
	
	print "Evaluating: ", $blastfile, "\n";
	if ($blastfile =~ m/^G\#(\w+)\_A\#(\w+\_*\S*)\_T\#(\w+\_*\w*\S*)\_blast/) {
		$genus = $1;
		$antivenom = $2;
		$toxinfam = $3;
	}
	#print $chip, " ", $genus, " ", $antivenom, " ", $toxinfam, "\n";
	### open Toxin fasta file:
	my $toxinfile = $genus."_".$toxinfam.".fasta";
	#print "Toxinfile: ", $toxinfile, "\n";
	open (INT, '<', $toxindir.$toxinfile) or die "reason; $!\n";
	while (defined (my $lineT = <INT>)) {
		chomp $lineT;
		push(@toxin, $lineT);	### Store toxin header and sequence in an array
		#print "Toxinfile: ", $lineT, "\n";
	}
	#print @toxin, "\n";
	close INT;					

	open (IN, '<', $blastdir.$blastfile) or die "reason: $!\n";
	
	while (defined (my $line = <IN>)) {
		chomp $line;
		#### Evaluate one alignment at the time:

## Each alignment starts with:
	# BLASTP 2.6.0+
	# Query: 
	# Database: 
	# Fields: query acc.ver, subject acc.ver, % identity, alignment length, mismatches, gap opens, q. start, q. end, s. start, s. end, evalue, bit score

	## remove header lines and alignments not of 100 % identity:
		
		if ($line =~ m/^#/) {	#header line
			#next;
		} elsif ($line =~ m/^\w{2}\|+/) {	#data line
			my @tmp = split("\t", $line);
			#print "data 0: ", $tmp[0], "\n";
			if ($tmp[2] == '100.000' && $tmp[4] == '0' && $tmp[5] == '0' && $tmp[3] == '16') {
				# % identity = 100; gap = 0; mismatch = 0; alignment legth = 16
				
				$pfam_new = $1 if $tmp[0] =~ m/^\w{2}\|(\S+)\|\S+/;	## match uniprot ID
				#print "Pfam_new: ", $pfam_new, "\n";
				if ($pfam_new eq $pfam_old) {	## Evaluating the same alignment, store information
					push(@data, $tmp[0], $tmp[1], $tmp[6], $tmp[7]);
						# query, subject, start, stop
				} else {	## Started new alignment: evaluate earlier
					if (scalar(@data) > 5) {	## only evaluate if new alignment is med, avoid analysis of empty array
						
						for (my $j = 0; $j < $#toxin; $j = $j+2) {	## Evaluate all possible proteins
							#print "evaluate toxins\n";
							my $uniprot = $1 if $toxin[$j] =~ m/^\>\w{2}\|(\S+)\|\w+/;
							
							if ($uniprot eq $pfam_old) {
								
								if ($toxin[$j] =~ m/^\>\w{2}\|\S+\|\w+\_*\w*\s{1}(\S+\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*)\s{1}OS\={1}(\w+\s{1}\w+)\s{1}[A-Z]{2}\=/) {
									$protinfo = $1;
									$species = $2;
									
								} elsif ($toxin[$j] =~ m/^\>\w{2}\|\S+\|\w+\_*\w*\s{1}(\S+\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*)\s{1}OS\={1}(\w+\s{1}\w+)\s{1}(\w*)\s{1}[A-Z]{2}\=/) {
									$protinfo = $1;
									$species = $2;
									$subspecies = $3;
								}
							 
								$sequence = $toxin[$j+1];	## Store full toxin sequence
								#print length($sequence), " ", $sequence, "\n";
								
						#### Assign protein sub and sub-sub family
								open(INM, '<', $mapfile) or die "reason: $!\n";
								while (defined (my $line = <INM>)) {
									chomp $line;
									my @tmp = split("\t", $line);
									
									if ($tmp[0] eq $uniprot) {
										if (scalar(@tmp) == 2) {
											$sub = "NA";
											$subsub = "NA";
										} elsif (scalar(@tmp) == 3) {
											$sub = $tmp[2];
											$subsub = "NA";
										} elsif (scalar(@tmp) == 4) {
											$sub = $tmp[2];
											$subsub = $tmp[3];
										}
									}
			
								}
								close INM;
							}
						}
						
				### Compute binding profile:
						
						for (my $i = 0; $i < scalar(@data); $i = $i+4) {
	
							$pos = $data[$i+2];
							$aa = substr($sequence, $pos-1, $kmer);
					
							$signal = $1 if $data[$i+1] =~ m/^\w+\_(\-{0,1}\d+\S+)$/;
							$species =~ s/ /_/g;
							
							
							#Header: "UniprotID\tGenus\tSpecies\tProteinFam\tProteinSubFam\tProteinSubSubFam\tAntivenom\tPosition_start\tPosition_end\tK_mer\tSignal\tUniprot\n";
							print OUT $pfam_old, "\t", $genus, "\t", $species, "\t", $toxinfam, "\t", $sub, "\t", $subsub, "\t", $antivenom,"\t", $pos, "\t", $pos+15, "\t", $aa, "\t", $signal, "\t", $protinfo, "\t";
							print OUT $toxinfam."_".$genus, "\t", $toxinfam."_".$species, "\t", $sub."_".$genus, "\t", $sub."_".$species, "\t", $subsub."_".$genus, "\t", $subsub."_".$species, "\n"; 
						}			
						@data = ();
					}
					push(@data, , $tmp[0], $tmp[1], $tmp[6], $tmp[7]);
					$pfam_old = $pfam_new;
					
					($id, $align, $gap, $mis, $sequence, $protinfo, $species, $sub, $subsub) = ('', '', '', '', '', '', '', '', '');
				}
			}
		}
	} ## End of blast file
	
	close IN;
	$blastfile = '';
	#@toxin = ();
#=put
	if (scalar(@data) > 5) {	## only evaluate if new alignment is med, avoid analysis of empty array
		#my $protein = $1 if $data[0] =~ m/^\w{2}\|(\S+)\|\w+/;	## Identify what toxin is used in the alignment
		#print "Hello -> print alignment\n";				
		for (my $j = 0; $j < $#toxin; $j = $j+2) {
			#print "evaluate toxins, last in file\n";
			
			my $uniprot = $1 if $toxin[$j] =~ m/^\>\w{2}\|(\S+)\|\w+/;
			#print "toxin protein, last in file: ", $uniprot, "\t old: ", $pfam_old, "\n";
								
			if ($uniprot eq $pfam_old) {
				
				#print $toxin[$j], "\n";
				#if ($toxin[$j] =~ m/^\>\w{2}\|\S+\|\w+\_*\w*\s+(\S+\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*)\s+OS\={1}(\w+\s{1}\w+)\s*\w*\s{1}PE/) {
				if ($toxin[$j] =~ m/^\>\w{2}\|\S+\|\w+\_*\w*\s{1}(\S+\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*\s*\S*)\s{1}OS\={1}(\w+\s{1}\w+)\s{0,1}\w*\s{1}[A-Z]{2}\=/) {
								
					$protinfo = $1;
					$species = $2;
					#print "INFO: ", $protinfo, "\tSnake: ", $species, "\n";
				}
								
								 
				$sequence = $toxin[$j+1];	## Store full toxin sequence
				#print length($sequence), " ", $sequence, "\n";
				open(INM, '<', $mapfile) or die "reason: $!\n";
				while (defined (my $line = <INM>)) {
					chomp $line;
					my @tmp = split("\t", $line);
								
					if ($tmp[0] eq $uniprot) {
						if (scalar(@tmp) == 2) {
							$sub = "NA";
							$subsub = "NA";
						} elsif (scalar(@tmp) == 3) {
							$sub = $tmp[2];
							$subsub = "NA";
						} elsif (scalar(@tmp) == 4) {
							$sub = $tmp[2];
							$subsub = $tmp[3];
						}
					}
									
									
											
				}
				close INM;

			}

		}
						
		### Compute binding profile:
			
		for (my $i = 0; $i < scalar(@data); $i = $i+4) {
	
			$pos = $data[$i+2];
			$aa = substr($sequence, $pos-1, $kmer);
					
			$signal = $1 if $data[$i+1] =~ m/^\w+\_(\-{0,1}\d+\.\d+)$/;
			$species =~ s/ /_/g;
							
			#Header: "UniprotID\tGenus\tSpecies\tProteinFam\tProteinSubFam\tProteinSubSubFam\tAntivenom\tPosition_start\tPosition_end\tK_mer\tSignal\tUniprot\n";
			print OUT $pfam_old, "\t", $genus, "\t", $species, "\t", $toxinfam, "\t", $sub, "\t", $subsub, "\t", $antivenom,"\t", $pos, "\t", $pos+15, "\t", $aa, "\t", $signal, "\t", $protinfo, "\t";
			print OUT $toxinfam."_".$genus, "\t", $toxinfam."_".$species, "\t", $sub."_".$genus, "\t", $sub."_".$species, "\t", $subsub."_".$genus, "\t", $subsub."_".$species, "\n"; 				
		}

	}
#=cut
	@data = ();
	($blastfile, $pfam_new, $pfam_old) = ('', '', '');
	($id, $align, $gap, $mis, $chip, $genus, $antivenom, $toxinfam, $sequence, $protinfo, $species) = ('', '', '', '', '', '', '', '', '', '', '');
	@toxin = ();
	($chip, $genus, $antivenom, $toxinfam, $sub, $subsub) = ('', '', '', '', '', '');
	

	
}

close OUT;



