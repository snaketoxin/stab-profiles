#!/usr/bin/perl -w
use strict;

###############################
# May 2018, Carina Skaarup, skaarup@bioinformatics.dtu.dk
#
# INPUT: toxin fasta files
# INPUT: Map files [uniprot id and corresponding protein family, sub-family and sub-sub family]
# INPUT: List of protein families to be evaluated
# OUTPUT: Fasta files, specific for namke genus and protein family
#
################################

## INPUT variables:
my $mapfile = "/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/toxins/toxin_map2.txt";
my $fastadir = "/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/toxins/";
my $pfamfile = "/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/toxins/Pfamlist.txt";	#Pfam_selected.txt";

###### ----------- STORE fasta file names --------------
opendir(DH, "$fastadir") or die "reason: $!\n";
my @tmpdir = readdir(DH);
closedir DH;

my @genuslist = ();
foreach my $file (@tmpdir) {
	push(@genuslist, $file) if $file =~ m/\_trimmed\.fsa$/;
}
@tmpdir = ();
#print @genuslist, "\n";

######## ----- list of protein families:
my @pfamlist = ();
open(INP, '<', $pfamfile) or die "reason: $!\n";
while (defined (my $line = <INP>)) {
	chomp $line;
	push(@pfamlist, $line);
}
close INP;
#print @pfamlist, "\n";

###### --- mapping list of uniprot ID and protein family 
my @maplist = ();
open(INM, '<', $mapfile) or die "reason: $!\n";
while (defined (my $line = <INM>)) {
	chomp $line;
	my @tmp = split("\t", $line);
	push(@maplist, $tmp[1], $tmp[0]);		
}
close INM;

my ($genus, $fam, $sub, $subsub) = ('', '', '', '');
my ($outfile, $mapfam, $fileopen) = ('', '', '0');
my @idlist = ();
my @genusfasta = ();
my @uniprotlist = ();
my $flag = '0';
####### Evaluate one genus at a time:

for (my $i = 0; $i < scalar(@genuslist); $i++) {
	$genus = $1 if $genuslist[$i] =~ m/^(\S+)\.fsa$/;
	print "# -------- \t", $genus, "\n";
	
	######## Evaluate current genus for all listed protein families:
	open (ING, '<', $fastadir.$genus.".fsa") or die "reason: $!\n";	
	
	while (defined (my $line = <ING>)) {
		chomp $line;
		push(@genusfasta, $line);
	
	}
	close ING;
	

	#### ------------ Evaluate one protein family at a time:
	for (my $j = 0; $j < scalar(@pfamlist); $j++) {
		$fam = $pfamlist[$j];
		#print $fam, "\n";
		#### Construct list of uniprot id's corresponding to the current protein family: 
		## Evaluate maplist:
		$outfile = $genus."_".$1.".fasta" if $fam =~ m/^(\S+)\_family$/;
		for (my $k = 0; $k < scalar(@maplist); $k= $k+2) {
			$mapfam = $maplist[$k];
			
			if ($mapfam eq $fam) {
				push(@uniprotlist, $maplist[$k+1]);
				#print "protein fam: ", $mapfam, " Uni ID: ", $maplist[$k+1], "\n";
				#print $maplist[$k+1], "\n";
			
			}
		} # End of map list
				
		for (my $l = 0; $l < scalar(@genusfasta); $l++) {
			
			if ($genusfasta[$l] =~ m/^\>\w{2}\|(\S+)\|/) {
				my $id = $1;
				#print $id, "\n";
				if ($flag == '1') {
					print OUT "\n";
					#print "End of protein\n";
				}
				$flag = 0;
				for (my $m = 0; $m < scalar(@uniprotlist); $m++) {
					my $uniid = $uniprotlist[$m];
					if ($id eq $uniid) {
						#print "Match: ", $id, "\t", $uniid, "\n";
						if ($fileopen == 0) {
							open (OUT, '>', "/home/projects/cu_10121/data/Pan_Africa/shiny/toxins/".$outfile) or die "reason: $!\n";
							$fileopen = 1;
							#print "New File\n";
						}
					
						print OUT $genusfasta[$l], "\n";	## print header line
						$flag = 1;
						#print "Protein header: ", $genusfasta[$l], "\n";
					
					}
				}
			} else {
				
				if ($flag == 1) {
					print OUT $genusfasta[$l];
					#print "Sequence line\n";	
				
				}
			}
		}
		@uniprotlist = ();
		$flag = 0;
		
		if ($fileopen == '1') {
			close OUT;
			#print "close file\n";
		}
		$fileopen = 0;
		
		
	} ## End of protein family
	@genusfasta = ();
	
		
} ## end of genus		
		
