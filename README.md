# STAB profiles 
- This folder is for files and scripts to be implemented in the Pan Africa Shiny app.

# Project background

The data has been generated with the aim to explore and understand the interactions between and recognition of snake venom toxins
by immunised animal antivenom serum (antibodies).

Chip design:
Peptide microarray chips have been synthetically synthesised with peptides of known toxins.
Each toxin is used to generate overlapping K-mers (ex.):
     LKCHNTQLPFIYKTC
      KCHNTQLPFIYKTCP
       CHNTQLPFIYKTCPE
        HNTQLPFIYKTCPEG

Peptides are randomly scattered over the array, and a selected number of peptides are present in five replicates.
Replicates are favorable to avoid local extremes as a result of noise, and to provide a more generalised score of the 
interaction signal.
The chip also include probes with alanine scan.

Analysis method:
The data has been generated using Roche NibleGen Technology.

# DATA:
The dataset is composed of three chip analysis, each chip composed of 12 microarrays.
On each chip 8 commercial antivenoms and negaive controls have been tested on an identical set of peptide probes.

# Normalisation
The chip is composed of selected protein peptides, replicates and random peptides.

## N1: Subtract background signal
Signal intensity observed in the sector evaluating negative control buffer is used to define background signal
and subtracted from signal intensities observed in sectors evaluating antivenom interactions.

## N.2: Normalise signal intensity - compute Z-score
Signal from random peptides from all antivenom sectors across the chip is first logtransformed and used to compute chip mean and standard diviation.

Normalisation is performed for all non-random peptides
1: log transform 
2: compute Z-score

## N3: Evaluate replicates
Each probe sequence is evaluated and replicates are assigned the median signal score


## N4: Combine norm scores
Antivenom specific signal scores are combined across the analysed chips to create one list of signals per peptide probe per antivenom.

# Local alignment
To assign the peptide signal value to a specific toxin alignment is perfomed for the chip probes angainst snake toxins of interest

## A1: Peptide fasta files
Peptide signals and sequence is converted into a fastaformst

## A2: Toxin fasta files
Toxin fasta files are updated in species and protein family specific lists

## A3: Alignment 
Peptide probes and toxin sequences are aligned
Creating one file for each Snake species, antivenom, and protein family combination

# Compute binding profile
Aligment files are evaluated to create a data file for plotting
Each line represent a specific peptide and signal score for a specific antivenom and protein entry




