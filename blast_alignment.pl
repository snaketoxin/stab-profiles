#!/usr/bin/perl -w
use strict;

my $dir_peptide = "/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/peptide/";
my $dir_toxin = "/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/toxins/";

#####------------------- Read peptide signal files -----------------###
opendir(DH, "$dir_peptide") or die "reason: $!\n";
my @tmpdir = readdir(DH);
closedir DH;

my @filelist = ();
foreach my $file (@tmpdir) {
	push(@filelist, $file) if $file =~ m/\_minusBuffer\.fasta$/;
}
@tmpdir = ();

#####-------------------- Read toxin fasta files -------------------####
opendir(DH, "$dir_toxin") or die "reason: $!\n";
my @tmpdir2 = readdir(DH);
closedir DH;

my @toxinlist = ();
foreach my $file (@tmpdir2) {
	push(@toxinlist, $file) if $file =~ m/\.fasta$/;
}
@tmpdir2 = ();

#### ------------ Evaluate all combinations
my ($filePep, $filetoxin, $antivenom, $genus, $toxin, $output) = ('', '', '', '', '', '');
my @cmd = ();
for (my $i = 0; $i < scalar(@filelist); $i++) {	 # evaluate one antivenom at a time	#scalar(@filelist); $i++) { 	#2; $i++) {			#Evaluate one file at a time
	$filePep = $filelist[$i];
	#my $id = $1 if $filePep =~ m/^(\d+\_+A\d{2})\.fasta/;
	$antivenom = $1 if $filePep =~ m/^(\S+)\_minusBuffer\.fasta/;
	
	for (my $j = 0; $j < scalar(@toxinlist); $j++) {		#scalar(@toxinlist); $j++) {	#1; $j++) {	
		$filetoxin = $toxinlist[$j];
		if ($filetoxin =~ m/^\b([A-Z][a-z]+)\_{1}(\S+)\.fasta$/) {
			$genus = $1;
			$toxin = $2;
			print "Genus: ", $genus, " toxin: ", $toxin, "\n";
		}
		
		#$output = "/home/projects/cu_10121/data/Pan_Africa/shiny/test/alignment/".$id."_G#".$genus."_A#".$antivenom."_T#".$toxin."_blast.txt";
		$output = "/home/projects/cu_10121/data/Pan_Africa/ANALYSIS_AUGUST/alignment/"."G#".$genus."_A#".$antivenom."_T#".$toxin."_blast.txt";
		
		print "processing: ", $filePep, " vs. ", $filetoxin, "\n";
		@cmd = ("blastp", "-query", $dir_toxin.$filetoxin, "-subject", $dir_peptide.$filePep, "-out", $output, "-outfmt", "7");
		system(@cmd);
	}

	($filePep, $filetoxin, $antivenom, $genus, $toxin, $output) = ('', '', '', '', '', '');
	@cmd = ();
}
